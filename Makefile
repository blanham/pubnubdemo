CFLAGS=-Wall -ggdb3 -O3 -std=gnu99 `pkg-config --cflags libpubnub`
LIBS=`pkg-config --libs libpubnub` -lpthread


all: server client

server: server.o
	gcc server.o $(LIBS) -o server	

client: client.o
	gcc client.o $(LIBS) -o client	

clean:
	rm -f *.o server
