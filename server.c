#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <string.h>

#include <semaphore.h>
#include <event.h>
#include <json.h>

#include <pubnub.h>
#include <pubnub-libevent.h>

//Terminal Control Codes for Color
#define BLUE "\33[1;34m"
#define RED "\33[0;31m"
#define REDB "\33[1;31m"
#define GREEN "\33[0;32m"
#define GREENB "\33[1;32m"
#define WHITE "\33[0;37m"
#define RESET "\33[0;m"
#define INV_BLUE "\33[;30;46m"
#define TIMEOUT 60

struct pubnub *p;
char *channel = "my_channel";

struct event server_query_timer;
struct timeval interval = { .tv_sec = 1, .tv_usec = 0};

int n = 0;
sem_t sem;


struct entry {
	int n;	
	struct json_object *msg;
	char *name;
	time_t t;
	SIMPLEQ_ENTRY(entry) entries;
};

SIMPLEQ_HEAD(list, entry) head = SIMPLEQ_HEAD_INITIALIZER(head);	

static void received(struct pubnub *p, enum pubnub_res result, char **channels, 
struct json_object *msg, void *ctx_data, void *call_data);

//Would be better to use curses for terminal control
//but is probably overkill in this case
#define PRINT_LINE(color) printf(color "%i\t%-20.20s\t%s %s" WHITE, e->n, e->name, sub_time, ctime(&timeout))
void server_query(int fd, short something, void *ctx)
{
	struct entry *e;
	time_t timeout, t = time(NULL);

	
	sem_wait(&sem);

	if(system("clear"));
	
	t = time(NULL);
	
	printf(INV_BLUE "Current time: %s" RESET, ctime(&t));
	printf(BLUE "Number\tName\t\t\tSubmitted Time\t\t  Expire Time\n" WHITE);
	
	if(!SIMPLEQ_EMPTY(&head))
	{
		SIMPLEQ_FOREACH(e, &head, entries)
		{
			char *sub_time =  ctime(&(e->t));
			
			timeout = e->t + TIMEOUT;
		
			sub_time = strdup(sub_time);
			sub_time[strlen(sub_time) - 1] = ' ';
			
			if(timeout - (TIMEOUT/4) < t)
				PRINT_LINE(RED);
			else if(timeout - (TIMEOUT/4)*2 < t)
				PRINT_LINE(REDB);
			else if(timeout -(TIMEOUT/4)*3 < t)
				PRINT_LINE(GREEN);
			else if(timeout - TIMEOUT < t)
				PRINT_LINE(GREENB);
			else
				PRINT_LINE(GREENB);

			if(timeout <= t)
			{
				SIMPLEQ_REMOVE_HEAD(&head, entries);
			}

		}
	}
	
	fflush(stdout);

	sem_post(&sem);
	
	evtimer_add(&server_query_timer, &interval);
}

static void received(struct pubnub *p, enum pubnub_res result, char **channels, 
struct json_object *msg, void *ctx_data, void *call_data)
{
	struct entry *new = malloc(sizeof(*new));
	if(result != PNR_OK)
	{
		exit(-1);
	}

	//Takes care of erroneus empty messages	
	if(strlen(json_object_to_json_string(msg)) < 4)
	{
		pubnub_subscribe(p, channel, -1, received, NULL);
		return;
	}

	new->n = n++;
		
	if (json_object_array_length(msg) != 0)
	{	
		json_object *msg1 = json_object_array_get_idx(msg, 0);
			
		new->t = json_object_get_int(json_object_object_get(msg1, "time"));
		new->name = (char *)json_object_get_string(json_object_object_get(msg1, "name"));
		
		if(new->name == NULL)
			new->name = strdup("(null)");
		else
			new->name = strdup(new->name);
			
		json_object_put(msg1);

	}

	json_object_put(msg);
	
	sem_wait(&sem);
	SIMPLEQ_INSERT_TAIL(&head, new, entries);
	sem_post(&sem);

	pubnub_subscribe(p, channel, -1, received, NULL);
}

int main(int argc, char **argv)
{
	event_init();

	p = pubnub_init("demo","demo",&pubnub_libevent_callbacks, pubnub_libevent_init());

	if(sem_init(&sem, 1, 1) != 0)
		return -1;

	pubnub_subscribe(p, channel, -1, received, NULL);


	evtimer_set(&server_query_timer, server_query, NULL);
	evtimer_add(&server_query_timer, &interval);

	event_dispatch(); 

	return 0;
}

