#include <stdio.h>
#include <time.h>

#include <event.h>
#include <json.h>

#include <pubnub.h>
#include <pubnub-sync.h>

//#define DEBUG
#define NAME_LEN 20

char *channel = "my_channel";

int main(int argc, char **argv)
{
	struct pubnub *p;
	struct pubnub_sync *sync;
	time_t t; 
	char *name = malloc(NAME_LEN + 1);;
	json_object *msg;

	sync = pubnub_sync_init();
	p = pubnub_init("demo", "demo", &pubnub_sync_callbacks, sync);
	
	#ifdef DEBUG
	printf("pubnub uuid: %s\n", pubnub_current_uuid(p ));
	#endif

	printf("Name: ");
	fgets(name, NAME_LEN, stdin);
	name[strlen(name) - 1] = ' ';
	msg = json_object_new_object();
	json_object_object_add(msg, "name", json_object_new_string(name));

	t = time(NULL);
	json_object_object_add(msg, "time", json_object_new_int(t));

	pubnub_publish(p, channel, msg, -1, NULL, NULL);

	json_object_put(msg);

	if (pubnub_sync_last_result(sync) != PNR_OK)
		return EXIT_FAILURE;

	#ifdef DEBUG
	msg = pubnub_sync_last_response(sync);
	printf("pubnub publish ok: %s\n", json_object_get_string(msg));
	json_object_put(msg);
	#endif

	pubnub_done(p);
	return 0;
}

